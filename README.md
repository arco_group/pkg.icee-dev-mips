Please, compile on 32bits, as the OpenWRT does not work well on 64bits.

To make a patch:

    $ diff -bBrupN original modified > openwrt.patch

To apply the patch:

    $ patch -d DIR -p1 < openwrt.patch